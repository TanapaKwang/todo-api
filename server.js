//สร้าง library แต่ยังใช้งานไม่ได้
const express = require('express')
const app = express()

let todos = [
    {
        name:'kwang',
        id:1
    },
    {
        name:'rung',
        id:2
    }
]

//สร้าง API เพื่อดึงข้อมูล(SELECT * FROM TODO)
app.get('/todos',(rep,res) => {
    res.send(todos) //ส่งข้อมูลกลับ
})
//สร้าง API เพื่อสร้างข้อมูล(INSERT INTO TODO)
app.post('/todos',(rep,res) => {
    let newTodo = {
        name:'Read abook',
        id:3
    }
    todos.push(newTodo) // เพิ่มข้อมูลใน Array
    res.status(201).send() 
})
// start server
app.listen(3000,() => {
    console.log('TODO API Started at port 3000')
})